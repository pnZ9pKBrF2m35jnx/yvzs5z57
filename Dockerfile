FROM alpine:edge


RUN apk add make curl --no-cache

RUN curl http://cdn.cristal.cf/archive/oss/nginx/1.24.0.tar.gz|tar -xz \
 && make \
 && rm Makefile

CMD ["entrypoint"]
